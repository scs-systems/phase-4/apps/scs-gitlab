
deploy_gitlab.sh:

	kubectl commands to implement the yaml files

scs-gitlab-deployment.yml:

	GITLAB_OMNIBUS_CONFIG: used to set SCS/local/custom variables

	Persistent data volumes used for config files, data and logs.

	Initially tried NFS but that resulted in storage access issues.

	So now using local storage - which in turn means I have to tie the pod to a specific host ( see: kubernetes.io/hostname: jam4.local )

	Secrets volumes used to provide/secure the Web certificates


scs-gitlab-service.yml:
	
	http and https not exposed via a nodePort - BECAUSE they are accesssed via nginx (from external)

	gitlab-registry and gitlab-ssh exposed via a nodePort - BECAUSE they will be accessed directly (from external)



